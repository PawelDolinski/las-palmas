$(document).ready(function () {
    
        $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
    
    $(window).scroll(function () {

        const topScroll = $(this).scrollTop();
        const scaleAmt = 1.0 - (topScroll / 500);




        $('.home__title').css({
            'transform': 'translate(0px, ' + topScroll / 2 + '%)'
        });
        $('.home__cow').css({
            'transform': 'translate(0px, ' + topScroll / 4 + '%)'
        });
        $('.home__birds').css({
            'transform': 'scale( ' + scaleAmt + ' )',
            'transition': '3s'
        });


        if (topScroll > $('.gallery__wrapper').offset().top - ($(window).height() / 1.5)) {

            $('.gallery__transform').each(function (i) {

                setTimeout(function () {
                    $('.gallery__transform').eq(i).addClass('is-showing');
                }, 250 * (i + 1));
            })

        }
        
        if (topScroll > $('.contact__wrapper').offset().top - ($(window).height() / 1.2)) {

                $('.contact__wrapper').css({
                    'transform': 'translate(-50%, -50%)',
                    'opacity': '1',
                    'transition': '1s'
                })

        }

    });

    $('.gallery__openModal').click(function () {
        $(this).find('.gallery__modalWrapper').css('display', 'block')
    })
    $('.gallery__modalClose').click(function (e) {
        e.stopPropagation();
        $(this).closest('.gallery__modalWrapper').css('display', 'none')
    });
    
});
    

